import socket

HOST = '127.0.0.1'
PORT = 8080

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()

print(f"Server listening on {HOST}:{PORT}")

conn, addr = server_socket.accept()

number = 1

while number <= 100:
    conn.sendall(str(number).encode())
    print(f"Server sent: {number}")

    data = conn.recv(1024).decode()
    if data.isdigit():  # เพิ่มเงื่อนไขตรวจสอบความถูกต้องของข้อมูล
        number = int(data) + 1
        print(f"Server received: {data}")
    else:
        print("Invalid data received. Closing connection.")
        break

conn.close()

